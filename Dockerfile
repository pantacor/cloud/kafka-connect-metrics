FROM golang as builder

COPY . /go/src/gitlab.com/pantahub/cloud-3rdparty/kafka-connect-metrics
RUN cd /go/src/gitlab.com/pantahub/cloud-3rdparty/kafka-connect-metrics \
    && CGO_ENABLED=0 GOOS=linux go build -a -ldflags '-extldflags "-static"' .

FROM scratch
COPY --from=builder /go/src/gitlab.com/pantahub/cloud-3rdparty/kafka-connect-metrics/kafka-connect-metrics /kafka-connect-metrics
CMD ["/kafka-connect-metrics"]
