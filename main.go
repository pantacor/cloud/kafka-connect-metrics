package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"
)

const (
	STATE_RUNNING = "RUNNING"
	STATE_PAUSED  = "PAUSED"
)

var (
	apiAddr            = os.Getenv("API_ADDRESS")
	kafkaConnectorAddr = os.Getenv("KAFKA_CONNECTOR_ADDRESS")
)

type Connector struct {
	State    string `json:"state"`
	WorkerID string `json:"worker_id"`
}

type ConnectorTask struct {
	State    string `json:"state"`
	ID       int    `json:"id"`
	WorkerID string `json:"worker_id"`
}

type ConnectorStatus struct {
	Name      string          `json:"name"`
	Connector Connector       `json:"connector"`
	Tasks     []ConnectorTask `json:"tasks"`
	Type      string          `json:"type"`
}

func metricsHandler(w http.ResponseWriter, _ *http.Request) {
	connectorsAddr := fmt.Sprintf("%s/connectors", kafkaConnectorAddr)
	resp, err := http.Get(connectorsAddr)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if resp.StatusCode != http.StatusOK {
		http.Error(w, "invalid response from remote kafka connector api", resp.StatusCode)
		return
	}

	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)

	var connectors []string
	err = decoder.Decode(&connectors)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	metrics := ""
	for _, connector := range connectors {
		connectorAddr := fmt.Sprintf("%s/connectors/%s/status", kafkaConnectorAddr, connector)
		resp, err := http.Get(connectorAddr)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		if resp.StatusCode != http.StatusOK {
			http.Error(w, "invalid response from remote kafka connector api", resp.StatusCode)
			return
		}

		decoder = json.NewDecoder(resp.Body)

		var status ConnectorStatus
		err = decoder.Decode(&status)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		runningStatus := 0
		if status.Connector.State == STATE_RUNNING || status.Connector.State == STATE_PAUSED {
			runningStatus = 1
		}

		metrics += fmt.Sprintf("kafka_connector_running{connector=\"%s\"} %d\n", status.Name, runningStatus)

		for _, task := range status.Tasks {
			taskRunningStatus := 0
			if task.State == "RUNNING" {
				taskRunningStatus = 1
			}

			metrics += fmt.Sprintf("kafka_connector_task_running{connector=\"%s\",task_id=\"%d\"} %d\n", status.Name, task.ID, taskRunningStatus)
		}

		resp.Body.Close()
	}

	w.Write([]byte(metrics))
}

func healthHandler(w http.ResponseWriter, _ *http.Request) {
	resp, err := http.Get(kafkaConnectorAddr)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if resp.StatusCode != http.StatusOK {
		http.Error(w, "invalid response from remote kafka connector api", resp.StatusCode)
		return
	}

	w.Write([]byte("OK"))
}

// LoggingMiddleware is a middleware function that logs details of incoming requests
func LoggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Log the request details
		log.Printf("[%s] %s %s", time.Now().Format("2006-01-02 15:04:05"), r.Method, r.URL.Path)

		// Call the next handler function in the chain
		next.ServeHTTP(w, r)
	})
}

func main() {

	timeout, err := time.ParseDuration(os.Getenv("TIMEOUT_DURATION"))
	if err != nil {
		timeout = 30 * time.Second
	}

	log.Printf("Using %s as timeout\n", timeout)

	metricsFunc := http.HandlerFunc(metricsHandler)
	healthFunc := http.HandlerFunc(healthHandler)

	http.Handle("/metrics", LoggingMiddleware(metricsFunc))
	http.Handle("/health", LoggingMiddleware(healthFunc))

	log.Printf("Started listenning at %s", apiAddr)
	log.Fatal(http.ListenAndServe(apiAddr, nil))
}
