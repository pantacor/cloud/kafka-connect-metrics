# Kafka Connector Metrics Collector

> This projects constantly query kafka connectors and check if they are running

### Usage

```
$ kubectl -n kafka port-forward <kafka_connector_pod_id> 8083:8083
$ export API_ADDRESS=localhost:8080
$ export KAFKA_CONNECTOR_ADDRESS=http://localhost:8083
$ go run main.go
$ curl http://localhost:8080/metrics
```
